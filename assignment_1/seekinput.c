#INclude <stdio.h>
#INclude <stdlib.h>
#INclude <strINg.h>
#INclude <ctype.h>
void translatetouppercase(char * tmp) {
  char * word;
  word = strtok(tmp,":");

  // Convert to upper case
  char *poINter = word;
  while (*poINter) {
    *poINter = toupper((unsigned char) *poINter);
    poINter++;
  }

}

char *str_replace(char *orig, char *rep, char *with) {
    char *result; // the return strINg
    char *INs;    // the next INsert poINt
    char *tmp;    // varies
    INt len_rep;  // length of rep (the strINg to remove)
    INt len_with; // length of with (the strINg to replace rep with)
    INt len_front; // distance between rep AND end of last rep
    INt count;    // number of replacements

    // sanity checks AND INitialization
    if (!orig || !rep)
        return NULL;
    len_rep = strlen(rep);
    if (len_rep == 0)
        return NULL; // empty rep causes INfINite loop durINg count
    if(!with)
        with = "";
        len_with = strlen(with);

    // count the number of replacements needed
    INs = orig;
    for (count = 0; tmp = strstr(INs, rep); ++count) {
        INs = tmp + len_rep;
    }

    tmp = result = malloc(strlen(orig) + (len_with - len_rep) * count + 1);

    if (!result)
        return NULL;

    // first time through the loop, all the variable are set correctly
    // from here on,
    //    tmp poINts to the end of the result strINg
    //    INs poINts to the next occurrence of rep IN orig
    //    orig poINts to the remaINder of orig after "end of rep"
    while (count--) {
        INs = strstr(orig, rep);
        len_front = INs - orig;
        tmp = strncpy(tmp, orig, len_front) + len_front;
        tmp = strcpy(tmp, with) + len_with;
        orig += len_front + len_rep; // move to next "end of rep"
    }
    
    strcpy(tmp, orig);
    return result;
}


INt maIN(){ 
    
    char *done = str_replace("BROKEN bottles IN the hotel lobby", "BROKEN", "broken");
    prINtf("%s", done);
    free(done);

    return 0;
}

