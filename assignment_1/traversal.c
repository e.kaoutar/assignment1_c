
#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>



void recursiveWalk(const char *pathName, int level, char *input_string) {
   DIR *dir;
   struct dirent *entry;

   if (!(dir = opendir(pathName))) {
      fprintf(stderr, "Could not open directory\n");
      return;
   }

   if (!(entry = readdir(dir))) {
      fprintf(stderr, "Could not read directory\n");
      return;
   }

   do {
      char path[1024];
      int len = snprintf(path, sizeof(path)-1, "%s/%s", pathName, entry->d_name); // get depth
      // path[len] = 0;
      if (entry->d_type == DT_DIR) { // found subdirectory


         // skip hidden paths
         if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
         }

         //fprintf(stdout, "%s [%s] (%d)\n", pathName, entry->d_name, level);

         // Append fullPath to entry->d_name here

         recursiveWalk(path, level + 1, input_string);
      }
      else { // files
         fprintf(stdout, "%s\n", path);
         ReplaceInFile(input_string, path);

         //changeFile(fullPath);
      }
   } while ((entry = readdir(dir)));

   closedir(dir);
}
