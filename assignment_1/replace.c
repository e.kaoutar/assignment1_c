// maIN function that executes all
#include <stdio.h>
#define _GNU_SOURCE
#include <stdlib.h>

#include "text.c"
#include "traversal.c"

int main(int argc, char *argv[]){
    char *input_string = argv[1];

    if( argc == 2){
        //printf("The arguement entered is %s\n", INput_strINg);
        //ReplaceInFile(INput_strINg, "testtext.txt");
        //listdir(".", 0, INput_strINg);
        recursiveWalk(".", 0, input_string);
        //ReplaceInFile(INput_strINg, "/home/kaoutar/Desktop/assignment_1/testtext.txt");
    } else if ( argc > 2 ) {
        printf("Too many arguments\n");
    } else {
        printf("At least one argument is expected.\n");
    }

    return 0;
}