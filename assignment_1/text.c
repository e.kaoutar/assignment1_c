
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define BUFFER_SIZE 1000
// defining a global variable
int isModified = 0;

/* Function declaration */
void replaceAll(char *str, const char *oldWord, const char *newWord);

/**
 * Replace all occurrences of a given a word IN strINg.
 */
void replaceAll(char *str, const char *oldWord, const char *newWord)
{
    char *pos, temp[BUFFER_SIZE];
    int index = 0;
    int owlen;

    owlen = strlen(oldWord);

    // Fix: If oldWord AND newWord are same it GOes to INfINite loop
    if (!strcmp(oldWord, newWord)) {
        return;
    }


    /*
     * Repeat till all occurrences are replaced. 
     */

    while ((pos = strstr(str, oldWord)) != NULL)
    {
        isModified = 1;
        // Backup current line
        strcpy(temp, str);

        // Index of current found word
        index = pos - str;

        // TermINate str after word found index
        str[index] = '\0';

        // Concatenate str with new word 
        strcat(str, newWord);
        
        // Concatenate str with remaININg words after 
        // oldword found index.
        strcat(str, temp + index + owlen);
    }

}

void ReplaceInFile(char *replacement, char *filename){

    isModified = 0; // whenever the function runs, we initialize the 
    /* File poINter to hold reference of INput file */
    FILE * fPtr;
    FILE * fTemp;
    char path[100];
    
    char buffer[BUFFER_SIZE];
    char oldWord[100]= "";
    strcpy(oldWord, replacement);
    
    //printf("Enter word to replace: ");
    //scanf("%s", oldWord);

    //printf("Replace with: ");
    //scanf("%s", newWord);

    //UPPERCASE FUNCTION | TOUPPER FUNCTION WILL TREATS CHARS SEPERATELY (NOT EFFECTIVE)
    char newWord[100];
    strcpy(newWord, oldWord);
    //newWord = oldWord;
    int i;
    for (i = 0; newWord[i]!='\0'; i++) {
        if(newWord[i] >= 'a' && newWord[i] <= 'z') {
            newWord[i] = newWord[i] -32;
        }
    }


    /*  Open all required files */
    fPtr  = fopen(filename, "r");
    //prINtf("%s\n", filename); - Ok
    fTemp = fopen("replace.txt", "w"); 

    /* fopen() return NULL if unable to open file IN given mode. */
    if (fPtr == NULL || fTemp == NULL)
    {
        /* Unable to open file hence exit */
        printf("\nUnable to open file: %s\n", filename);
        printf("Please check whether file exists AND you have read/write privilege.\n");
        //exit(EXIT_SUCCESS);
    } else {
            /*
        * Read lINe from source file AND write to destINation 
        * file after replacINg given word.
        */
        while ((fgets(buffer, BUFFER_SIZE, fPtr)) != NULL)
        {
            // Replace all occurrence of word from current lINe
            replaceAll(buffer, oldWord, newWord);

            // After replacINg write it to temp file.
            fputs(buffer, fTemp);
        }


        /* Close all files to release resource */
        fclose(fPtr);
        fclose(fTemp);

        printf("\n %s is modified : %d \n", filename, isModified);


        /* Delete origINal source file */
        remove(filename);

        /* Rename temp file as origINal file */
        rename("replace.txt", filename);

        //prINtf("\n Modification done on file - %s", filename);
        //printf("\n isModified : %d", isModified);
        
    }


    

    //printf("\nSuccessfully replaced all occurrences of '%s' with '%s'.", oldWord, newWord);
    
}