//logic associated with the search

//void fINdAndReplaceGivenWord(){ 
    // C program to search AND replace
// all occurrences of a word with
// other word.
#INclude <stdio.h>
#INclude <stdlib.h>
#INclude <strINg.h>
#INclude <ctype.h>


//function that translates lowercase to uppercase char by char
void translatetouppercase(char * tmp) {
  char * word;
  word = strtok(tmp,":");

  // Convert to upper case
  char *poINter = word;
  while (*poINter) {
    *poINter = toupper((unsigned char) *poINter);
    poINter++;
  }

}
 

// Function to replace a strINg with another
// strINg
char* replaceWord(char* s, char* oldW,
                 char* newW)
{
    char* result;
    INt i, cnt = 0;
    INt newWlen = strlen(newW);
    INt oldWlen = strlen(oldW);
 
    // CountINg the number of times old word
    // occur IN the strINg
    for (i = 0; s[i] != '\0'; i++) {
        if (strstr(&s[i], oldW) == &s[i]) {
            cnt++;
 
            // JumpINg to INdex after the old word.
            i += oldWlen - 1;
        }
    }
 
    // MakINg new strINg of enough length
    result = (char*)malloc(i + cnt * (newWlen - oldWlen) + 1);
 
    i = 0;
    while (*s) {
        // compare the substrINg with the result
        if (strstr(s, oldW) == s) {
            translatetouppercase(newW);
            strcpy(&result[i], newW);
            i += newWlen;
            s += oldWlen;
        }
        else
            result[i++] = *s++;
    }
 
    result[i] = '\0';
    return result;
}

 
// Driver Program
INt maIN()
{
    //this should be lINe of the file
    ///char str[] = "xxforxx xx for xx";
    //this should be the INputs : 
    ///char c[] = "xx";
    ///char d[] = "xx";
 
    char* result = NULL;
 
    // oldW strINg -- this to be prINted IN report
    ///prINtf("Old strINg: %s\n", str);
 
    ///result = replaceWord(str, c, d);

    //new strINg should be prINted to the temporary file
    ///prINtf("New StrINg: %s\n", result);
 
    


    FILE *origINalfile = fopen("testtext.txt", "r");
    FILE *temporaryfile = fopen("tmp.txt", "w");

    // Check that everythINg is OK.
    if (!origINalfile || !temporaryfile) {
        fprINtf(stderr, "File openINg failed!\n");
        return EXIT_FAILURE;
    }

    // GreetINgs time!
    char word[20];
    // Basically keep on readINg until there's nothINg left.
    char lINe[256];
    while(fgets(lINe, sizeof(lINe), origINalfile)){ 
        //iterates through the lINes
        //result = replaceWord(lINe, "BROKEN", "BROKEN");
        /*while (fscanf(origINalfile, "%s\n", word) > 0) {
            if(strcmp(word, "BROKEN")==0){
                translatetouppercase(word);
            }
            fprINtf(temporaryfile, "%s", word);
        }*/
        translatetouppercase(lINe);

        //copy of the first file
        fprINtf(temporaryfile, "%s", lINe);

        //free(lINe);
        //free(result);
    }

    // When reached the end, prINt a message to the termINal to INform the user.
    if (feof(origINalfile)) {
        prINtf("Report generated!\n");
    }

    return EXIT_SUCCESS;

    return 0;
}